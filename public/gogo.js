let graph = new joint.dia.Graph();
let paper = new joint.dia.Paper({
    el: document.getElementById('paper'),
    width: 1900,
    height: 900,
    model: graph,
    async: true,
    snapLabels: true,
    interactive: {
        elementMove: false,
        labelMove: true,
        arrowheadMove: false},
    defaultConnectionPoint: {
        name: 'boundary',
        args: {
            extrapolate: true,
            sticky: true
        }
    },
    validateConnection: function() {
        return false;
    }
});

let addRelation = document.getElementById(`addRelation`);
let addRelationCreationModal = document.getElementById(`addRelationCreationModal`);
let spanRelationCreationModal = document.getElementById(`closeRelationCreationModal`);
let relationName = document.getElementById(`relationCreationModalName`);

let relationClassList = document.getElementById("relationClassList");

let countOfStroke;

addRelation.onclick = function (){
    countOfStroke = 0
    addRelationCreationModal.style.display = "block";
    document.body.style.position = 'fixed';
    document.body.overflowY = 'hidden';

    relationName.focus();

    while (relationClassList.firstChild) {
        relationClassList.removeChild(relationClassList.firstChild);
    }

    let tr = document.createElement("tr");
    let thName = document.createElement("th");
    let thType = document.createElement("th");

    thName.innerHTML = "Имена классов";
    thType.innerHTML = "Типы классов"
    tr.appendChild(thName);
    tr.appendChild(thType);
    relationClassList.appendChild(tr);

    for(let i = 1; i < localStorage.getItem('x'); i++) {
        let tr = document.createElement("tr");
        tr.setAttribute('id', `${i}`);
        let tdName = document.createElement("td");
        let tdType = document.createElement("td");

        tdName.innerHTML = localStorage.getItem(`name ${i}`);
        tdType.innerHTML = localStorage.getItem(`Type ${i}`);
        tr.appendChild(tdName);
        tr.appendChild(tdType);
        relationClassList.appendChild(tr);
    }

    relationClassList.onclick = function(e){
        let stroke = e.target.closest('tr');

        if (!stroke.classList.contains('selected') && countOfStroke !== 2) {
            stroke.classList.toggle('selected');
            stroke.classList.toggle(`${countOfStroke}`);
            countOfStroke++;
        }else if(stroke.classList.contains('selected')){
            stroke.classList='';
            countOfStroke--;
        }
    };
}
spanRelationCreationModal.onclick = function() {
    addRelationCreationModal.style.display = "none";

    const wrapObj = document.querySelectorAll('.selected');
    for(let i = 0; i<wrapObj.length;i++) {
        wrapObj[i].classList.remove('selected');
    }
}

let modifyRelationModal = document.getElementById('modifyRelationModal');
let modifyRelation = document.getElementById('modifyRelation');
let spanRelationModifyModal = document.getElementById('closeRelationModifyModal');
modifyRelation.onclick = function() {
    modifyRelationModal.style.display = 'block';
}
spanRelationModifyModal.onclick = function() {
    modifyRelationModal.style.left = 0 + 'px';
    modifyRelationModal.style.top = 0 + 'px';
    modifyRelationModal.style.display = 'none';
}

let createRelationButton = document.getElementById('saveRelation');
let currentId, currentId0, currentId1;
createRelationButton.addEventListener('click', createLineBetweenRelation);

let elements = [], el, relationBetween = [];
let RB, nameOfRelation;

if(localStorage.getItem(`countOfRelation`)!==null) {
    RB = parseInt(localStorage.getItem(`countOfRelation`));
}  else {
    RB = 0;
}

function createLineBetweenRelation() {

    for (let i = 0; i < document.getElementsByClassName('selected').length; i++) {
        if(document.getElementsByClassName('selected').length === 2) {
            if (document.getElementsByClassName('selected')[i].classList.contains(`0`)) {
                relationBetween[0] = document.getElementsByClassName('selected')[i].id;
            }
            if (document.getElementsByClassName('selected')[i].classList.contains(`1`)) {
                relationBetween[1] = document.getElementsByClassName('selected')[i].id;
            }
            console.log("Yes, i have");
            console.log(currentId0);
            console.log(currentId1);
            el = document.getElementById(`Concept ${currentId}`);
        }else if(document.getElementsByClassName('selected').length === 1){
            relationBetween[0] = document.getElementsByClassName('selected')[i].id
            relationBetween[1] = document.getElementsByClassName('selected')[i].id
        }
    }

    console.log(relationBetween[0][0]);

    RB++;

    if (document.getElementById(`relationCreationModalName`).value !== '') {
        localStorage.setItem(`RelationName ${RB}`, `${document.getElementById(`relationCreationModalName`).value}`);
        nameOfRelation = document.getElementById(`relationCreationModalName`).value;
    } else {
        localStorage.setItem(`RelationName ${RB}`, `Relation ${RB}`);
        nameOfRelation = `Relation ${RB}`;
    }

    localStorage.setItem(`RelationBetween ${RB}`, `${relationBetween}`);
    localStorage.setItem(`countOfRelation`, RB);

    console.log(relationBetween[0])

    drawLine(relationBetween[0], relationBetween[1], RB);

    const wrapObj = document.querySelectorAll('.selected');
    for(let i = 0; i<wrapObj.length;i++) {
        wrapObj[i].classList.remove('selected');
    }

    addRelationCreationModal.style.display = "none";
}

let temp;

function normalizeMarker(d, offset) {
    let path = new g.Path(V.normalizePathData(d));
    let bbox = path.bbox();
    let ty = - bbox.height / 2 - bbox.y;
    let tx = - bbox.width / 2 - bbox.x;
    if (typeof offset === 'number') tx -= offset;
    path.translate(tx, ty);
    return path.serialize();
}

let links = [], massiveOfLinks = [];
let s = [], tempCoordinate;
let original = [];
let massiveOfConcepts = [];

for(let i = 1; i < parseInt(localStorage.getItem('x')); i++)
{
    tempCoordinate = {
        x: document.getElementById(`Concept ${i}`).getBoundingClientRect().x,
        y: document.getElementById(`Concept ${i}`).getBoundingClientRect().y - 73,
        width: document.getElementById(`Concept ${i}`).getBoundingClientRect().width,
        height: document.getElementById(`Concept ${i}`).getBoundingClientRect().height
    }

    original[i] = new joint.shapes.standard.Rectangle({
        position: {
            x: tempCoordinate.x,
            y: tempCoordinate.y,
        },
        size: {
            width: tempCoordinate.width,
            height: tempCoordinate.height
        },
    });
    original[i].attr({
        body: {
            rx: 10,
            ry: 10,
            strokeWidth: 0
        }
    })
    massiveOfConcepts.push(original[i]);
}

function drawLine(id1, id2, numberOfLink) {
    for(let i = 1; i < parseInt(localStorage.getItem('x')); i++)
    {
        s[i] = {
            x: document.getElementById(`Concept ${i}`).getBoundingClientRect().x,
            y: document.getElementById(`Concept ${i}`).getBoundingClientRect().y - 73,
            width: document.getElementById(`Concept ${i}`).getBoundingClientRect().width,
            height: document.getElementById(`Concept ${i}`).getBoundingClientRect().height
        }

        original[i].attributes.position.x = s[i].x;
        original[i].attributes.position.y = s[i].y;

        massiveOfConcepts.push(original[i]);
    }

    graph.addCell(massiveOfConcepts);

    if (id1 !== id2) {
        links[numberOfLink] = new joint.shapes.standard.Link({
            source: {id: original[id1].id},
            target: {id: original[id2].id},
            connector: {name: 'smooth'},
            attrs: {
                line: {
                    stroke: '#000000',
                    strokeWidth: 3,
                    sourceMarker: {
                        'stroke': '#000000',
                        'fill': '#000000',
                    },
                    targetMarker: {
                        'd': 'M 15 -7 -2 0 15 7 Z',
                        'stroke': '#000000',
                        'fill': '#000000',
                    }
                }
            }
        });
        links[numberOfLink].appendLabel({
            markup: [{
                tagName: 'rect',
                selector: 'labelBody'
            }, {
                tagName: 'text',
                selector: 'labelText'
            }],
            attrs: {
                relId: numberOfLink,
                labelText: {
                    text: localStorage.getItem(`RelationName ${numberOfLink}`),
                    fill: '#000000',
                    fontFamily: 'sans-serif',
                    textAnchor: 'middle',
                    textVerticalAnchor: 'middle'
                },
                labelBody: {
                    ref: 'labelText',
                    refX: -5,
                    refY: -5,
                    refWidth: '100%',
                    refHeight: '100%',
                    refWidth2: 10,
                    refHeight2: 10,
                    stroke: '#000000',
                    fill: 'white',
                    strokeWidth: 2,
                    rx: 5,
                    ry: 5
                }
            }
        });

        console.log(numberOfLink);

        graph.addCell(links[numberOfLink]);

        links[numberOfLink].toBack();

        graph.on('change:position', function () {
            links[numberOfLink].findView(paper).requestConnectionUpdate();
        });
    }else if(id1===id2) {
        links[numberOfLink] = new joint.shapes.standard.Link({
            source: {id: original[id1].id},
            target: {id: original[id2].id},
            vertices: [{x:s[id1].x - 50, y:s[id1].y - 50}],
            connector: {name: 'smooth'},
            attrs: {
                line: {
                    stroke: '#000000',
                    strokeWidth: 3,
                    sourceMarker: {
                        'stroke': '#000000',
                        'fill': '#000000',
                    },
                    targetMarker: {
                        'd': 'M 15 -7 -2 0 15 7 Z',
                        'stroke': '#000000',
                        'fill': '#000000',
                    }
                }
            }
        });
        links[numberOfLink].appendLabel({
            markup: [{
                tagName: 'rect',
                selector: 'labelBody'
            }, {
                tagName: 'text',
                selector: 'labelText'
            }],
            attrs: {
                relId: numberOfLink,
                labelText: {
                    text: localStorage.getItem(`RelationName ${numberOfLink}`),
                    fill: '#000000',
                    fontFamily: 'sans-serif',
                    textAnchor: 'middle',
                    textVerticalAnchor: 'middle'
                },
                labelBody: {
                    ref: 'labelText',
                    refX: -5,
                    refY: -5,
                    refWidth: '100%',
                    refHeight: '100%',
                    refWidth2: 10,
                    refHeight2: 10,
                    stroke: '#000000',
                    fill: 'white',
                    strokeWidth: 2,
                    rx: 5,
                    ry: 5
                }
            }
        });

        console.log(numberOfLink);

        graph.addCell([original[id1], original[id2], links[numberOfLink]]);

        links[numberOfLink].toBack();

        graph.on('change:position', function () {
            links[numberOfLink].findView(paper).requestConnectionUpdate();
        });
    }
}

function updateLine() {
    graph.clear();
    massiveOfLinks=[];
    for (let i = 1; i < parseInt(localStorage.getItem('countOfRelation')) + 1; i++) {
        for(let k = 1; k < parseInt(localStorage.getItem('x')); k++) {
            s[k] = {
                x: document.getElementById(`Concept ${k}`).getBoundingClientRect().x,
                y: document.getElementById(`Concept ${k}`).getBoundingClientRect().y - 73,
                width: document.getElementById(`Concept ${k}`).getBoundingClientRect().width,
                height: document.getElementById(`Concept ${k}`).getBoundingClientRect().height
            }

            original[k].attributes.position.x = s[k].x;
            original[k].attributes.position.y = s[k].y;
        }

        for (let j = 0; j < 2; j++) {
            temp = localStorage.getItem(`RelationBetween ${i}`).split(',')[j];
            elements[j + 1] = {
                id: parseInt(temp)
            }
        }

        let massiveOfVertices;
        if(links[i].vertices !== undefined) {
            massiveOfVertices = links[i].vertices();
        }

        let source = original[elements[1].id];
        let target = original[elements[2].id];

        links[i] = new joint.shapes.standard.Link({
            source: {id: source.id},
            target: {id: target.id},
            vertices: massiveOfVertices,
            connector: {name: 'smooth'},
            attrs: {
                line: {
                    stroke: '#000000',
                    strokeWidth: 3,
                    sourceMarker: {
                        'stroke': '#000000',
                        'fill': '#000000',
                    },
                    targetMarker: {
                        'd': 'M 15 -7 -2 0 15 7 Z',
                        'stroke': '#000000',
                        'fill': '#000000',
                    }
                }
            }
        });
        links[i].appendLabel({
            markup: [{
                tagName: 'rect',
                selector: 'labelBody'
            }, {
                tagName: 'text',
                selector: 'labelText'
            }],
            attrs: {
                relId: i,
                labelText: {
                    text: localStorage.getItem(`RelationName ${i}`),
                    fill: '#000000',
                    fontFamily: 'sans-serif',
                    textAnchor: 'middle',
                    textVerticalAnchor: 'middle'
                },
                labelBody: {
                    ref: 'labelText',
                    refX: -5,
                    refY: -5,
                    refWidth: '100%',
                    refHeight: '100%',
                    refWidth2: 10,
                    refHeight2: 10,
                    stroke: '#000000',
                    fill: 'white',
                    strokeWidth: 2,
                    rx: 5,
                    ry: 5
                }
            }
        });

        links[i].toBack();
    }

    massiveOfLinks = [];
    for(let i = 1; i < parseInt(localStorage.getItem('x')); i++) {
        s[i] = {
            x: document.getElementById(`Concept ${i}`).getBoundingClientRect().x,
            y: document.getElementById(`Concept ${i}`).getBoundingClientRect().y - 73,
            width: document.getElementById(`Concept ${i}`).getBoundingClientRect().width,
            height: document.getElementById(`Concept ${i}`).getBoundingClientRect().height
        }

        original[i].attributes.position.x = s[i].x;
        original[i].attributes.position.y = s[i].y;

        massiveOfLinks.push(original[i]);
    }
    for(let i = 1; i < parseInt(localStorage.getItem('countOfRelation')) + 1;i++) {
        massiveOfLinks.push(links[i]);
    }

    graph.resetCells(massiveOfLinks);
}

paper.on('link:mouseenter', function(linkView) {
    let tools;
    tools = [
        new joint.linkTools.Vertices({
            snapRadius: 0,
            redundancyRemoval: false
        }),
    ];

    linkView.addTools(new joint.dia.ToolsView({
        name: 'onhover',
        tools: tools
    }));
});

paper.on('link:mouseleave', function(linkView) {
    if (!linkView.hasTools('onhover')) return;
    linkView.removeTools();
});

paper.on('myclick:circle', function(linkView, evt) {
    evt.stopPropagation();
    let link = linkView.model;
    let t = (link.attr('c1/atConnectionRatio') > .2) ? .2 :.9;
    let transitionOpt = {
        delay: 100,
        duration: 2000,
        timingFunction: joint.util.timing.inout
    };
    link.transition('attrs/c1/atConnectionRatio', t, transitionOpt);
    link.transition('attrs/c2/atConnectionRatio', t, transitionOpt);
});

let relationModalName = document.getElementById(`relationName`);
let saveRelationInModal = document.getElementById(`saveRelationInModal`);
let swapOrientation = document.getElementById(`swapOrientation`);

paper.on('link:contextmenu', function(linkView) {
    let currentLink = linkView.model;
    console.log(currentLink.attributes.labels[0].attrs.relId);
    let currentLinkId = currentLink.attributes.labels[0].attrs.relId;

    modifyRelationModal.style.display = 'block';
    relationModalName.value = localStorage.getItem(`RelationName ${currentLinkId}`)

    let firstElement, secondElement, numbersOfElementsInRelation, textLine;
    numbersOfElementsInRelation = localStorage.getItem(`RelationBetween ${currentLinkId}`).split(',');
    firstElement = localStorage.getItem(`name ${numbersOfElementsInRelation[0]}`);
    secondElement = localStorage.getItem(`name ${numbersOfElementsInRelation[1]}`);

    textLine = firstElement + " и " + secondElement + " находятся в отношении " + "\""+localStorage.getItem(`RelationName ${currentLinkId}`) + "\"";

    let writeDescription = document.getElementById("writeDescription");
    writeDescription.textContent = textLine;

    saveRelationInModal.onclick = function(){
        let relationName = document.getElementById(`relationChangeModalName`).value;
        if(relationName!=='')
        {
            localStorage.setItem(`RelationName ${currentLinkId}`, `${relationName}`);

            updateLine();

            modifyRelationModal.style.left = 0 + 'px';
            modifyRelationModal.style.top = 0 + 'px';
            modifyRelationModal.style.display = 'none';
        }else{
            modifyRelationModal.style.left = 0 + 'px';
            modifyRelationModal.style.top = 0 + 'px';
            modifyRelationModal.style.display = 'none';
        }
    }

    swapOrientation.onclick = function(){
        let massiveOfTempRelation = [], massiveOfRelation = [];

        massiveOfRelation = localStorage.getItem(`RelationBetween ${currentLinkId}`).split(`,`);
        massiveOfTempRelation[0] = massiveOfRelation[1]
        massiveOfTempRelation[1] = massiveOfRelation[0]

        localStorage.setItem(`RelationBetween ${currentLinkId}`, `${massiveOfTempRelation}`);
        updateLine();

        modifyRelationModal.style.left = 0 + 'px';
        modifyRelationModal.style.top = 0 + 'px';
        modifyRelationModal.style.display = 'none';
    }
});

if(parseInt(localStorage.getItem(`countOfRelation`))!==0) {
    for (let i = 1; i < RB + 1; i++) {
        for (let j = 0; j < 2; j++) {
            temp = localStorage.getItem(`RelationBetween ${i}`).split(',')[j];
            elements[j + 1] = {
                id: parseInt(temp)
            }
        }
        drawLine(
            elements[1].id,
            elements[2].id,
            i
        )
    }
}
